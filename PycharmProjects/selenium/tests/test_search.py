import json
import allure
import pytest
from pages.search_page import SearchPage
from allure_commons.types import AttachmentType


class TestSearch:

    @pytest.mark.parametrize('search_params', json.load(open('search_parameters.json', 'r', encoding='utf-8')))
    @allure.feature('Open page')
    @allure.step('Поиск фильма')
    def test_search(self, driver, search_params):
        search_page = SearchPage(driver, 'https://www.kinopoisk.ru/s/')
        search_page.open()
        search_page.search(**{k: v for k, v in search_params.items() if k in ['title', 'country', 'genre']})
        comparison = search_params['comparison']
        allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)
        with allure.step('Проверяем наличие фильма в топ 5'):
            search_page.get_top_5_films(comparison)
            allure.attach(driver.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)
