from selenium.webdriver.common.by import By


class FromPagesLocators:
    TITLE_INPUT = (By.XPATH, '//input[@id="find_film"]')
    COUNTRY_INPUT = (By.XPATH, '//select[@id="country"]')
    GENRE_INPUT = (By.XPATH, '//select[@class="text el_6 __genreSB__"]')
    SEARCH_BUTTON = (By.XPATH, '//input[@class="el_18 submit nice_button"]')
    SEARCH_RESULTS = (By.XPATH, '//p[@class="name"]//a')
