from pages.base_page import BasePage
from pages.frome_pages_locators import FromPagesLocators as Locators
from selenium.webdriver.support.ui import Select


class SearchPage(BasePage):

    def search(self, title, country, genre):
        self.driver.find_element(*Locators.TITLE_INPUT).send_keys(title)
        select_country = Select(self.driver.find_element(*Locators.COUNTRY_INPUT))
        select_country.select_by_visible_text(country)
        select_genre = Select(self.driver.find_element(*Locators.GENRE_INPUT))
        select_genre.select_by_visible_text(genre)
        self.driver.find_element(*Locators.SEARCH_BUTTON).click()

    def get_top_5_films(self, title):
        search_results = self.driver.find_elements(*Locators.SEARCH_RESULTS)
        assert len(search_results) >= 5, f'Expected at least 5 search results, but got {len(search_results)}'
        titles = []
        for i in search_results:
            titles.append(i.text)
        assert title in titles, f'Title "{title}" not found in search results'
        top_five_titles = titles[:5]
        assert title in top_five_titles, f'Title "{title}" not found in top 5 search results'
